<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_lengkap', 'nik', 'tgl_lahir', 'jk', 'alamat', 'telp', 'email','created_at','updated_at',
    ];
    protected $table ='tb_peserta';
    protected $primaryKey = 'nik';

}
