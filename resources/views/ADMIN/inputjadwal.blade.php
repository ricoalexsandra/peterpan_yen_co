@extends('layouts.appadmin_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
<<<<<<< HEAD:resources/views/ADMIN/inputjadwal.blade.php
            	<div class="card-header mb-2"> <h4> <b> Jadwal Tes </b> </h4> </div>

            <div class="container">
                <div class="form-floating">
                  <form method="post" action="{{route('inputjadwal')}}" enctype='multipart/form-data'>
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
=======
            	<div class="card-header mb-3"><h4><b> Data Jadwal Tes </b></h4></div>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
>>>>>>> 8362e4829313ccf2ab7db935f898ea109e159c20:resources/views/inputjadwal.blade.php
                    @endif

                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                    @endif

                    @if(\Session::has('Forbidden'))
                            <div class="alert alert-danger">
                                <p>{{\Session::get('Forbidden')}}</p>
                            </div>
                    @endif
              <div class="container">
                    <blockquote class="blockquote">
                        <a href="/tambahinputjadwal" class="btn btn-outline-primary"> Jadwal Tes [+] </a>
                    </blockquote>
                    <table class="table table-hover">
                        <tr>
                            <th> No </th>
                            <th> Jenis Tes </th>
                            <th> Tanggal Pelaksanaan </th>
                            <th> Jam Pelaksanaan </th>
                            <th> Link Pertemuan </th>
                            <th> Kapasitas </th>
                            <th> Action </th>
                        </tr>
                        <tr>
                            @foreach($injadwal as $jd)
                                <td> {{ $loop->iteration }} </td>
                                <td> {{ DB::table('tb_jenis_tes')->where('kode_tes', $jd->kode_tes)->value('nama_tes') }} </td>
                                <td> {{ $jd->tgl_pelaksanaan }} </td>
                                <td> {{ $jd->jam_pelaksanaan }} </td>
                                <td> {{ $jd->link_pertemuan }} </td>
                                <td> {{ $jd->kapasitas }} </td>
                                <td> 
                                    <a href="/editinputjadwal/{{$jd->kode_jadwal}}" class="btn btn-warning"> Edit </a>
                                    <a href="/deleteinputjadwal/{{$jd->kode_jadwal}}" class="btn btn-danger"> Delete </a>
                                </td>
                        </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
