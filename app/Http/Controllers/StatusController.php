<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Detil;
use Auth;
use App\Models\Transaksi;

class StatusController extends Controller
{
    public function index(){
    	$auth = Auth::user()->id;
    	$detil_stat= Detil::where([
            ['id', '=', $auth],
            ])->get();

    	$detil_stat2 = Detil::where([
    		['id','=', $auth],
    	])->select('kode_detil')->get();
    	$convert = $detil_stat2[0]['kode_detil'];
   
    	$pem_stat4 = Transaksi::where([
    		['kode_detil','=', DB::table('tb_transaksi')->where('kode_detil', $convert)->value('kode_transaksi')],
    	])->get();

    	//return $pem_stat4;
    	return view('status',compact('detil_stat', 'pem_stat4'));
    }
}
