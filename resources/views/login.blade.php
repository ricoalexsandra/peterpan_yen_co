<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="mt-5 row justify-content-center">
            <div class="col-xs-12 col-md-4 col-xl-3">
                <h2 class="text-center">Test Psikologi Online</h2>
                <hr>
                <!-- @if (session()->has('message'))
                    <div class="alert alert-{{ array_keys(session()->get('message'))[0] }}">
                        <span>{{ session()->get('message')[array_keys(session()->get('message'))[0]] }}</span>
                    </div>
                @endif -->
                <form method="post" action="{{ route('loginform') }}">
                    @csrf
                    <div class="mb-3 form-group">
                        <input type="text" class="form-control" autofocus name="username" id="username" placeholder="Username" value="{{ old('username') }}">
                       <!--  @error('username')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror -->
                    </div>
                    <div class="mb-3 form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        <!-- @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror -->
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-success btn-block">Login</button>
                    </div>
                    <div class="mb-3 text-center">
                        <a href="#">Forgot Password?</a>
                    </div>
                    <div class="mb-3">
                        <a href="{{ url('register') }}" class="btn btn-danger btn-block">Registrasi</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
