@extends('layouts.appadmin')

@section('content')
<div class="container">
	<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header mb-3"><h4><b> Update Jadwal Tes </b></h4></div>
                <div class="container">
				 	<form method="post" action="/updateinputjadwal/{{ $edit_jd->kode_jadwal }}" enctype='multipart/form-data'>
				 		@csrf
                        @method('PUT')
				 		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
				 		<select class="form-control" name="JenisTes">
				 			<option disabled selected> Pilih Jenis Tes </option>
				      			@foreach($data as $jwl)
				      				<option value="{{ $edit_jd->kode_tes }}"> {{ DB::table('tb_jenis_tes')->where('kode_tes', $jwl->kode_tes)->value('nama_tes') }} </option>
				      			@endforeach
				      	</select> <br>
				        <div class="form-group">
				          	<label> Tanggal Pelaksanaan </label>
				          	<input type="date" class="form-control" name="TglPelaksanaan" value="{{ $edit_jd->tgl_pelaksanaan }}">
				        </div>
				        <div class="form-group">
				        	<label> Waktu </label>
				          	<input type="time" class="form-control" name="JamPelaksanaan" value="{{ $edit_jd->jam_pelaksanaan }}">
				        </div>
				        <div class="form-group">
				          	<label> Link Pertemuan </label>
				          	<input type="text" class="form-control" name="Link" value="{{ $edit_jd->link_pertemuan }}">
				        </div>
				        <div class="form-group">
				            <label> Kapasitas </label>
				            <input type="number" class="form-control" name="Kapasitas" value="{{ $edit_jd->kapasitas }}">
				        </div>
				        <button type="submit" class="btn btn-primary mb-3"> Update </button>
				  </form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection