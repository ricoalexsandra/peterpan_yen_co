@extends('layouts.appadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header mb-3"><h4><b> Update Data Jenis Tes </b></h4></div>
                <div class="container">
                    <form method="post" action="/updateinputtes/{{ $edit_jt->kode_tes }}" enctype='multipart/form-data'>
                        @csrf
                        @method('PUT')
                        @php
                            $fasilitas = array();
                            $fasilitas = explode(',', $edit_jt->fasilitas);
                        @endphp
                        <input type="hidden" name="id" class="form-control" value="{{ $edit_jt->kode_tes }} ">
                        
                        <div class="form-group">
                            <label><b> Nama Tes </b></label>
                            <input type="text" class="form-control" name="NamaTes" value="{{ $edit_jt->nama_tes }}">
                        </div>
                        <div class="form-group">
                            <label><b> Fasilitas </b></label> <br>
                            
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Sertifikat" <?php in_array('Sertifikat', $fasilitas) ? print 'checked':'' ?> > Sertifikat <br>
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Surat Keterangan" <?php in_array('Surat Keterangan', $fasilitas) ? print 'checked':'' ?>> Surat Keterangan Tes <br>
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Penjelasan Hasil Tes" <?php in_array('Penjelasan Hasil Tes', $fasilitas) ? print 'checked':'' ?>> Penjelasan Hasil Tes <br>
                        </div>
                        <div class="form-group">
                            <label><b> Biaya </b></label>
                            <input type="text" class="form-control" name="Biaya" value="{{ $edit_jt->biaya }}">
                        </div>
                        <div class="form-group">
                            <label><b> Keterangan Jenis Tes </b></label>
                            <textarea name="Keterangan" rows="3" class="form-control">{{ $edit_jt->keterangan }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mb-3"> Update </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection