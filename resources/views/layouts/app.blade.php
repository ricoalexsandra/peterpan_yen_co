<!DOCTYPE html>
  <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta name="viewport" content="{{ csrf_token() }}">
        <!-- Script -->
        <script>
            function openNav() {
              document.getElementById("mySidebar").style.width = "250px";
              document.getElementById("main").style.marginLeft = "250px";
            }

            function closeNav() {
              document.getElementById("mySidebar").style.width = "0";
              document.getElementById("main").style.marginLeft= "0";
            }
        </script>

        <!-- Style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('App\Models\Jadwal') }}">

        <!-- Script Modal -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <style>
            body {
                font-family: "Lato", sans-serif;
            }

            .sidebar {
              height: 100%;
              width: 0;
              position: fixed;
              z-index: 1;
              top: 0;
              left: 0;
              background-color: #111;
              overflow-x: hidden;
              transition: 0.5s;
              padding-top: 60px;
            }

            .sidebar a {
              padding: 8px 8px 8px 32px;
              text-decoration: none;
              font-size: 25px;
              color: #818181;
              display: block;
              transition: 0.3s;
            }

            .sidebar a:hover {
              color: #f1f1f1;
            }

            .sidebar .closebtn {
              position: absolute;
              top: 0;
              right: 25px;
              font-size: 36px;
              margin-left: 50px;
            }

            .openbtn {
              font-size: 20px;
              cursor: pointer;
              background-color: #111;
              color: white;
              padding: 10px 15px;
              border: none;
            }

            .openbtn:hover {
              background-color: #444;
            }

            #main {
              transition: margin-left .5s;
              padding: 16px;
            }

            /* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
            @media screen and (max-height: 450px) {
              .sidebar {padding-top: 15px;}
              .sidebar a {font-size: 18px;}
            }
        </style>
    </head>
    <body>
        <div id="mySidebar" class="sidebar">
            <h1 style="color: white;"> Welcome, {{ Auth::user()->name }} </h1> <span class="caret"> </span>
            <a class="closebtn" onclick="closeNav()"> × </a>
            <a href="{{ route('home') }}"> <span class="fa fa-drivers-license-o"> Beranda </a>
            <a href="{{ route('biodata') }}"> <span class="fa fa-address-book"> Biodata </span> </a>
            <a href="{{ route('jenistes') }}"><span class="fa fa-calendar"> Daftar Tes </a>
              
            <a href="{{ route('statusTes') }}"> <span class="fa fa-money"> Status Tes </a>
              @if(DB::table('tb_transaksi')->where('status_transaksi','=',0)==false)
                <a href="{{ route('pembayaran') }}"> <span class="fa fa-money"> Pembayaran </a>
              @endif

            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form>

        </div>
        <div id="main">
            <button class="openbtn" onclick="openNav()"> ☰ Menu </button>
            <main class="py-4">
                @yield('content')
            </main>
        </div>

    </body>
</html>
