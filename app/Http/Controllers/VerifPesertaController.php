<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Detil;
use App\Models\Jadwal;
use App\Models\Transaksi;
use Carbon\Carbon;

class VerifPesertaController extends Controller
{
    public function index(Request $req){
        Transaksi::create([
            'kode_detil' => $req->get('id'),
            'tgl_pemesanan' => Carbon::parse($req->get('tgl_pesan'))->format('y-m-d'),
            'tgl_pembayaran' => null,
            'file_pembayaran' => null,
            'metode_pembayaran' => null,
            'status_pembayaran' => 0,
            'total' => $req->get('total'),
        ]);

        Detil::where('kode_detil', $req->get('id'))->update(['status_tes' => 1]);
        return redirect()->route('daftarpeserta')->with('success','Data Verified');
    }

    public function tolak(Request $req){
    	Detil::where('kode_detil', $req->get('idTolak'))->update(['status_tes' => 2]);
        return redirect()->route('daftarpeserta')->with('Forbidden','Data Rejected');
    }
}
