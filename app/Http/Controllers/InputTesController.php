<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JenisTes;

class InputTesController extends Controller
{
    public function index(){
        $jenis_tes = JenisTes::all();
        return view('inputtes', compact('jenis_tes'));
    }

    public function tambah(){
       return view('tambahinputtes');
    }

    public function edit($kode_tes){
        $edit_jt = JenisTes::find($kode_tes);
        return view('editinputtes',['edit_jt' => $edit_jt]);
    }

    public function update($kode_tes, Request $req){
        $up_jt = JenisTes::find($kode_tes);
        $fasilitas = implode(",", $req->get('Fasilitas'));
            $up_jt->nama_tes = $req->get('NamaTes');
            $up_jt->fasilitas = $fasilitas;
            $up_jt->biaya = $req->get('Biaya');
            $up_jt->keterangan = $req->get('Keterangan');
            $up_jt->save();
        return redirect()->route('inputtes')->with('success','Data Updated');
    }

    public function delete($kode_tes){
        $del_jt = JenisTes::find($kode_tes);
        $del_jt->delete();
        return redirect()->route('inputtes')->with('forbidden','Data deleted');
    }

    public function create(Request $req){
    	$this->validate($req, [
            'NamaTes' => 'required',
            'Biaya' => 'required',
            'Keterangan' => 'required',
        ]);

    	if(!empty($req->input('Fasilitas'))){
    		$check = join(',', $req->input('Fasilitas'));
    	}else{
    		$check = "";
    	}

    	$JenisTes = new JenisTes([
	            'nama_tes' => $req->get('NamaTes'),
	            'fasilitas' => $check,
	            'biaya' => $req->get('Biaya'),
	            'keterangan' => $req->get('Keterangan')
        	]);

        $JenisTes->save();
        return redirect()->route('inputtes')->with('success','Data Added');
    }
}
