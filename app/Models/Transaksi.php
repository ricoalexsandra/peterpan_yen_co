<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaksi extends Model
{
    use HasFactory;
     protected $fillable = [
        'kode_transaksi','kode_detil', 'tgl_pemesanan', 'tgl_pembayaran', 'file_pembayaran', 'metode_pembayaran', 'total', 'status_pembayaran', 'created_at','updated_at',
    ];
    protected $table ='tb_transaksi';
    protected $primaryKey = 'kode_transaksi';

    public function getCreatedAtAttribute($value){
    	$date = new Carbon($value);
    	return $date->format('l, d-m-Y');
    }
}
