<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\JenisTes;

class JenisTesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jenistes = [
        	[
	        	'nama_tes' => 'Tes Grafis',
	        	'biaya' => '300000',
	        	'keterangan' => 'Bla bla bla bla Bla bla bla bla Bla bla bla bla',
        	],
        	[
	        	'nama_tes' => 'DISC',
	        	'biaya' => '450000',
	        	'keterangan' => 'Bla bla bla bla Bla bla bla bla Bla bla bla bla',
        	]
        ];

        foreach($jenistes as $key => $value){
        	JenisTes::create($value);
        }
    }
}
