@extends('layouts.appadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            	<div class="card-header"> <h4>Daftar Peserta Tes</h4></div>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif 

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif
                
                @if(\Session::has('Forbidden'))
                      <div class="alert alert-danger">
                          <p>{{\Session::get('Forbidden')}}</p>
                      </div>
              @endif

            <div class="container">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">NO</th>
                        <th scope="col">NAMA LENGKAP</th>
                        <!-- <th scope="col">NIK</th> -->
                        <!-- <th scope="col">TGL.LAHIR</th> -->
                        <th scope="col">JENIS KELAMIN</th>
                        <!-- <th scope="col">ALAMAT</th> -->
                        <th scope="col">NO.TELP</th>
                        <th scope="col">EMAIL</th>
                        <th scope="col">JENIS TES</th>
                        <th scope="col">JADWAL</th>
                        <th scope="col">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                         @foreach($detil as $no => $p)
                            
                      <tr>
                        <th scope="row"> {{ $loop->iteration }} </th>
                        <td>  
                            {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nama_lengkap') }} 
                        </td>
                        <!-- <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nik') }}
                        </td> -->
                        <!-- <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('tgl_lahir') }} 
                        </td> -->
                        <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('jk') }} 
                        </td>
                        <!-- <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('alamat') }} 
                        </td> -->
                        <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('telp') }} 
                        </td>
                        <td> 
                             {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('email') }} 
                        </td>
                        <td> 
                            {{DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', $p->kode_jadwal)->value('kode_tes'))->value('nama_tes')}} 
                        </td>
                        <td> 
                            {{ $p->getCreatedAtAttribute(DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('tgl_pelaksanaan')) }} 
                            {{ DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('jam_pelaksanaan') }} 
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{ $loop->iteration }}"> Verifikasi </button>

                            <!-- The Modal -->
                            <div class="modal fade" id="myModal{{ $loop->iteration }}">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                  
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h4 class="modal-title"> Yakin ingin Verifikasi ?</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <table class="table">
                                            <tr>
                                                <td><b> Nama Lengkap </b></td>
                                                <td> : </td>
                                                <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nama_lengkap') }} </td>
                                            </tr>
                                            <tr>
                                                <td><b> NIK  </b></td>
                                                <td> : </td>
                                                <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nik') }} </td>
                                            </tr>
                                            <tr>
                                                <td><b> Telp  </b></td>
                                                <td> : </td>
                                                <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('telp') }} </td>
                                            </tr>
                                            <tr>
                                                <td><b> Email  </b></td>
                                                <td> : </td>
                                                <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('email') }} </td>
                                            </tr>
                                            <tr>
                                                <td><b> Jenis Tes </b></td>
                                                <td> : </td>
                                                <td> {{DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', $p->kode_jadwal)->value('kode_tes'))->value('nama_tes')}} </td>
                                            </tr>
                                            <tr>
                                                <td><b> Jadwal </b></td>
                                                <td> : </td>
                                                <td> {{ DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('tgl_pelaksanaan') }} {{ DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('jam_pelaksanaan') }} </td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <form method="post" action="{{ route('verifikasiPeserta') }} ">

                                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                                            <input type="hidden" name="id" value="{{ $p->kode_detil }}">

                                            <input type="hidden" name="total" value="{{DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', $p->kode_jadwal)->value('kode_tes'))->value('biaya')}}" >
                                            
                                            <input type="hidden" name="tgl_pesan" value="{{ $p->created_at }} ">

                                            <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal </button>
                                            <button type="submit" class="btn btn-success"> Konfirmasi </button>
                                        </form>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1{{$loop->iteration}}"> Tolak </button>

                            <!-- The Modal -->
                            <div class="modal fade" id="myModal1{{$loop->iteration}}">
                            <div class="modal-dialog">
                              <div class="modal-content">
                              
                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h4 class="modal-title"> Yakin ingin Tolak ?</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <table class="table">
                                        <tr>
                                            <td><b> Nama Lengkap </b></td>
                                            <td> : </td>
                                            <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nama_lengkap') }} </td>
                                        </tr>
                                        <tr>
                                            <td><b> NIK  </b></td>
                                            <td> : </td>
                                            <td>  {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $p->id)->value('nik'))->value('nik') }} </td>
                                        </tr>
                                        <tr>
                                            <td><b> Jenis Tes </b></td>
                                            <td> : </td>
                                            <td> {{DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', $p->kode_jadwal)->value('kode_tes'))->value('nama_tes')}} </td>
                                        </tr>
                                        <tr>
                                            <td><b> Jadwal </b></td>
                                            <td> : </td>
                                            <td> {{ DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('tgl_pelaksanaan') }} {{ DB::table('tb_jadwal')->where('kode_jadwal',$p->kode_jadwal)->value('jam_pelaksanaan') }} </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <form method="post" action="{{ route('tolakPeserta') }} ">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <input type="hidden" name="idTolak" value="{{ $p->kode_detil }}">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal </button>
                                        <button type="submit" class="btn btn-success"> Konfirmasi </button>
                                    </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                        
                      @endforeach
                    </tbody>
                  </table>
                <p> Halaman : </p>
                  @php echo $detil->links() @endphp
            </div>
        </div>
    </div>
</div>
@endsection
