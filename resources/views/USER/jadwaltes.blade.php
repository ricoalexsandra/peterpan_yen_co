@extends('layouts.appuser_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
            	<div class="card-header"> Jadwal Tes Psikologi </div>
                
            <div class="container">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col" class="text-center">NO</th>
                        <th scope="col" class="text-center">TGL PELAKSANAAN </th>
                        <th scope="col" class="text-center">JAM PELAKSANAAN </th>
                        <th scope="col" class="text-center">JENIS TES</th>
                        <th scope="col" class="text-center"> KAPASITAS </th>
                        <th scope="col" class="text-center">ACTION</th>
                      </tr>
                        </thead>
                    <tbody>
                      @foreach($jadwal as $jw)
                      <tr>
<<<<<<< HEAD:resources/views/USER/jadwaltes.blade.php
                        <th scope="row">
                          {{ $jw->getCreatedAtAttribute($jw->tgl_pelaksanaan) }}
                        </th>
                        <td> {{ $jw->nama_tes }} </td>
                        <td> {{ $jw->kapasitas }} </td>
                        <td> {{ Session::get('kode_tes') }} </td>

=======
                        <th> {{ $loop->iteration }} </th>
                        <th> 
                          {{ $jw->getCreatedAtAttribute($jw->tgl_pelaksanaan) }} 
                        </th>
                        <th> 
                          {{ $jw->jam_pelaksanaan }} 
                        </th>
                        <td> 
                            {{ DB::table('tb_jenis_tes')->where('kode_tes', $jw->kode_tes)->value('nama_tes') }}
                        </td>                   
                        <td> {{ $jw->kapasitas }} </td>
>>>>>>> 8362e4829313ccf2ab7db935f898ea109e159c20:resources/views/jadwaltes.blade.php
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{ $loop->iteration }}"> Pilih Jadwal </button>

                            <!-- The Modal -->
                            <div class="modal fade" id="myModal{{ $loop->iteration }}">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                  
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h4 class="modal-title"> Konfirmasi Jadwal </h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <table class="table">
                                            <tr>
                                                <td> Jenis Tes  </td>
                                                <td> : </td>
                                                <td> {{ DB::table('tb_jenis_tes')->where('kode_tes', $jw->kode_tes)->value('nama_tes') }} </td>
                                            </tr>
                                            <tr>
                                                <td> Waktu </td>
                                                <td> : </td>
                                                <td> {{ $jw->getCreatedAtAttribute($jw->tgl_pelaksanaan) }} </td>
                                            </tr>
                                            <tr>
                                                <td> Fasilitas </td>
                                                <td> : </td>
                                                <td> {{ DB::table('tb_jenis_tes')->where('kode_tes', $jw->kode_tes)->value('fasilitas') }} </td>
                                            </tr>
                                            <tr>
                                                <td> Biaya </td>
                                                <td> : </td>
                                                <td> Rp. {{ DB::table('tb_jenis_tes')->where('kode_tes', $jw->kode_tes)->value('biaya') }} </td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <form method="post" action="{{ route('pilihJadwal') }}">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                                            <input type="hidden" name="id" value="{{ $jw->kode_jadwal }}">

                                            <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal </button>
                                            <button type="submit" class="btn btn-success"> Konfirmasi </button>
                                        </form>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
<<<<<<< HEAD:resources/views/USER/jadwaltes.blade.php

=======
>>>>>>> 8362e4829313ccf2ab7db935f898ea109e159c20:resources/views/jadwaltes.blade.php
                      </tr>
                    </tbody>
                    @endforeach
                  </table>

            </div>
        </div>
    </div>
</div>
@endsection
