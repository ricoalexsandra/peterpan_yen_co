<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>

    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="mt-5 row justify-content-center">
            <div class="col-xs-12 col-md-6 col-xl-4">
                <h2 class="text-center">Test Psikologi Online</h2>
                <hr>
                @if (session()->has('message'))
                    <div class="alert alert-{{ array_keys(session()->get('message'))[0] }}">
                        <span>{{ session()->get('message')[array_keys(session()->get('message'))[0]] }}</span>
                    </div>
                @endif
                <form method="post" action="{{ route('registerform') }}">
                    @csrf
                    <div class="mb-3 form-group row">
                        <label for="username" class="col-xs-12 col-md-4 col-xl-3 col-form-label">Username</label>
                        <div class="col-xs-12 col-md-8 col-xl-9">
                            <input type="text" class="form-control" autofocus name="username" id="username" placeholder="Masukkan Username" value="{{ old('username') }}">
                            @error('username')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 form-group row">
                        <label for="email" class="col-xs-12 col-md-4 col-xl-3 col-form-label">Email</label>
                        <div class="col-xs-12 col-md-8 col-xl-9">
                            <input type="text" class="form-control" autofocus name="email" id="email" placeholder="Masukkan Email" value="{{ old('email') }}">
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 form-group row">
                        <label for="notelp" class="col-xs-12 col-md-4 col-xl-3 col-form-label">No. Telp</label>
                        <div class="col-xs-12 col-md-8 col-xl-9">
                            <input type="text" class="form-control" autofocus name="notelp" id="notelp" placeholder="Masukkan No. Telp" value="{{ old('notelp') }}">
                            @error('notelp')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 form-group row">
                        <label for="password" class="col-xs-12 col-md-4 col-xl-3 col-form-label">Password</label>
                        <div class="col-xs-12 col-md-8 col-xl-9">
                            <input type="password" class="form-control" autofocus name="password" id="password" placeholder="Masukkan Password">
                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 form-group row">
                        <label for="confirmpassword" class="col-xs-12 col-md-4 col-xl-3 col-form-label">Konfirmasi Password</label>
                        <div class="col-xs-12 col-md-8 col-xl-9">
                            <input type="password" class="form-control" autofocus name="confirmpassword" id="confirmpassword" placeholder="Masukkan Konfirmasi Password">
                            @error('confirmpassword')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-success btn-block">Register</button>
                    </div>
                    <div class="mb-3 text-center">
                        <a href="{{ url('login') }}">Have an account?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
 -->