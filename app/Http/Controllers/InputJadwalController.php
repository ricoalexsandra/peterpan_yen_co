<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InputJadwal;
use App\Models\JenisTes;
use App\Models\Jadwal;

class InputJadwalController extends Controller
{
    public function index(){
    	$injadwal = Jadwal::all();
        
    	return view('inputjadwal', compact('injadwal'));
    }

    public function tambah(){
        $tambah_jd = JenisTes::all();
        return view('tambahinputjadwal', compact('tambah_jd'));
    }

    public function edit($kode_jadwal){
        $data = Jadwal::join('tb_jenis_tes', 'tb_jadwal.kode_tes', '=', 'tb_jenis_tes.kode_tes')->select('tb_jenis_tes.*')->distinct()->get();
        $edit_jd = Jadwal::find($kode_jadwal);
        return view('editinputjadwal',['edit_jd' => $edit_jd], compact('data'));
    }

    public function update($kode_jadwal, Request $req){
        $up_jd = Jadwal::find($kode_jadwal);
            $up_jd->kode_tes = $req->get('JenisTes');
            $up_jd->tgl_pelaksanaan = $req->get('TglPelaksanaan');
            $up_jd->jam_pelaksanaan = $req->get('JamPelaksanaan');
            $up_jd->link_pertemuan = $req->get('Link');
            $up_jd->kapasitas = $req->get('Kapasitas');
            $up_jd->save();
        return redirect()->route('inputjadwal')->with('success','Data Updated');
    }

    public function delete($kode_jadwal){
        $del_jd = Jadwal::find($kode_jadwal);
        $del_jd->delete();
        return redirect()->route('inputjadwal');
    }

    public function create(Request $req){
    	$this->validate($req, [
            'JenisTes' => 'required',
            'TglPelaksanaan' => 'required',
            'JamPelaksanaan' => 'required',
            'Kapasitas' => 'required',
        ]);

    	$Injadwal = new InputJadwal([
            'kode_tes' => $req->get('JenisTes'),
            'jam_pelaksanaan' => $req->get('JamPelaksanaan'),
            'link_pertemuan' => $req->get('Link'),
            'tgl_pelaksanaan' => $req->get('TglPelaksanaan'),
            'kapasitas' => $req->get('Kapasitas')
    	]);

        $Injadwal->save();
        return redirect()->route('inputjadwal')->with('success','Data Added');
    }
}
