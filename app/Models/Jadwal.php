<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Jadwal extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode_tes', 'nama_tes', 'biaya', 'keterangan', 'kode_jadwal', 'jam_pelaksanaan', 'tgl_pelaksanaan', 'link_pertemuan','kapasitas', 'created_at', 'updated_at',
    ];

    protected $table ='tb_jadwal';

    protected $primaryKey = 'kode_jadwal';

    public function getCreatedAtAttribute($value){
    	$date = new Carbon($value);
    	return $date->format('l, d-M-Y');
    }
}
