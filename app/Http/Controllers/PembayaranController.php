<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Detil;
use App\Models\Transaksi;
use App\Models\Jadwal;
use Auth;
use Carbon\Carbon;

class PembayaranController extends Controller
{
    public function index(){

    	/*$pem_stat = Transaksi::join('tb_detil_baru', 'tb_detil_baru.kode_detil','=','tb_transaksi.kode_detil')->select('tb_detil_baru.kode_jadwal')->get()->toArray();
    	$convert = $pem_stat[0]['kode_jadwal'];

    	$pem_stat2 = Detil::join('tb_jadwal', 'tb_jadwal.kode_jadwal','=','tb_detil_baru.kode_jadwal')->select('tb_jadwal.kode_tes','tb_jadwal.tgl_pelaksanaan', 'tb_jadwal.jam_pelaksanaan', 'tb_jadwal.link_pertemuan')->where('tb_jadwal.kode_jadwal', $convert)->where('tb_detil_baru.id', $auth)->get();
    	$convert2 = $pem_stat2[0]['kode_tes'];

    	$pem_stat3 = Jadwal::join('tb_jenis_tes', 'tb_jenis_tes.kode_tes','=','tb_jadwal.kode_tes')->select('tb_jenis_tes.nama_tes','tb_jenis_tes.biaya')->where('tb_jenis_tes.kode_tes', $convert2)->get();*/

        $auth = Auth::user()->id;
        $detil_stat= Detil::where([
            ['id', '=', $auth],
            ['status_tes', '=', 1]
            ])->get();

        $detil_stat2 = Detil::where([
            ['id','=', $auth],
        ])->select('kode_detil')->get();
        $convert = $detil_stat2[0]['kode_detil'];

        $pem_stat4 = Transaksi::where([
            ['kode_detil','=', DB::table('tb_transaksi')->where('kode_detil', $convert)->value('kode_transaksi')],
            ['status_pembayaran', '=', 0]
        ])->get();

    	return view('pembayaran',compact('detil_stat','pem_stat4'));
    }

    public function bayar(Request $req){
        $byr = Transaksi::find($req->get('kode'));
            $byr->tgl_pembayaran = Carbon::now();

            $fullname = $req->file('FileBayar')->getClientOriginalName();
            $nik=Auth::user()->nik;
            $extn =$req->file('FileBayar')->getClientOriginalExtension();
            $final= $nik.'_bayar'.'_'.time().'.'.$extn;
            $path = $req->file('FileBayar')->storeAs('public/bayar', $final);

            $byr->file_pembayaran = $final;
            $byr->metode_pembayaran = $req->get('MetodeBayar');
            $byr->status_pembayaran = 1;
            $byr->save();

    	return redirect()->route('pembayaran')->with('success','Payment Success');
    }
}
