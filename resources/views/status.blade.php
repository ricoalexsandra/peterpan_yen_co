@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header mb-3"> <h4><b> Status Tes </b></h4>
              <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif

                <div class="container">
                  <table class="table table-hover" style="text-align: center;">
                    <tr>
                        <th> No </th>
                        <th> Nama </th>
                        <th> Jenis Tes </th>
                        <th> Jadwal Pelaksanaan</th>
                        <th> Status </th>
                        @if(DB::table('tb_detil_baru')->where('status_tes', '=', '2')->value('status_tes'))
                          <th> Action </th>
                        @endif
                    </tr>
                    @foreach($detil_stat as $d)
                      @if(DB::table('users')->where('id', '!==', $d->id)->value('id'))
                         <tr><td> Belum Ada </td></tr>
                      @else
                          <tr>
                            <td> {{ $loop->iteration }} </td>
                            <td>  
                              {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', $d->id)->value('nik'))->value('nama_lengkap') }}
                            </td>
                            <td>  
                              {{ DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', $d->kode_jadwal)->value('kode_tes'))->value('nama_tes') }}
                            </td>
                            <td> 
                              {{ $d->getCreatedAtAttribute(DB::table('tb_jadwal')->where('kode_jadwal',$d->kode_jadwal)->value('tgl_pelaksanaan')) }} Pukul:{{ DB::table('tb_jadwal')->where('kode_jadwal',$d->kode_jadwal)->value('jam_pelaksanaan') }}  
                            </td>

                            @if($d->status_tes == 1)
                                <td><span class="fa fa-check" style="font-size:24px"></span></td>
                                <td> 
                                </td>
                            @elseif($d->status_tes == 2)
                                <td><span class="fa fa-close" style="font-size:24px"></span></td>
                                <td> 
                                  <a href="{{ route('jenistes') }}" class="btn btn-success"> Silahkan Ganti Jadwal ! </a>
                                </td>
                            @else
                                <td><span class="fa fa-clock-o" style="font-size:24px"></span></td>

                            @endif
                          </tr>
                      @endif
                      
                      @endforeach
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
            <div class="card">
              <div class="card-header mb-3"> <h4> <b> Status Pembayaran </b> </h4></div>
                <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif

                <div class="container">
                  <table class="table table-hover" style="text-align: center;">
                    <tr>
                        <th> No </th>
                        <th> Tanggal Pemesanan </th>
                        <th> Tanggal Pembayaran </th>
                        <th> Bukti Pembayaran </th>
                        <th> Metode Pembayaran </th>
                        <th> Total Bayar </th>
                        <th> Status </th>
                        @if(DB::table('tb_transaksi')->where('status_pembayaran', '=', '1')->value('status_pembayaran') || DB::table('tb_transaksi')->where('status_pembayaran', '=', '0')->value('status_pembayaran'))
                          <th> Action </th>
                        @endif
                    </tr>
                    @foreach($pem_stat4 as $ps)
                          <tr>
                            <td> {{ $loop->iteration }} </td>
                            <td> {{ $ps->getCreatedAtAttribute($ps->tgl_pemesanan) }} </td>
                            <td> {{ $ps->updated_at }} </td>
                            <td> {{ $ps->file_pembayaran }} </td>
                            <td> {{ $ps->metode_pembayaran }} </td>
                            <td> Rp. {{ number_format($ps->total) }} </td>

                            @if($ps->status_pembayaran == 2)
                                <td><span class="fa fa-check" style="font-size:24px"></span></td>
                                <td> 
                                  <a href="#" class="btn btn-primary"> Link Meeting </a>
                                </td>
                            @elseif($ps->status_pembayaran == 0 || $ps->status_pembayaran == 1)
                                <td><span class="fa fa-clock-o" style="font-size:24px"></span></td>
                                <td> 
                                  <a href="{{route('pembayaran')}}" class="btn btn-success"> Bayar </a>
                                </td>
                            @endif
                          </tr>
                      @endforeach
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
