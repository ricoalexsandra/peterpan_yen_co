<!DOCTYPE html>
<html>
<head>
    <title> </title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">  
            <div class="card">
                <div class="card-header"> Pendaftaran </div> <br>
                <div class="container"> 
                	<div class="form-group">
                        <form method="post" action="{{route('inpendaftaran')}}" enctype='multipart/form-data'>
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                <label> Nama Lengkap </label>
                                <input type="text" class="form-control" name="NamaLengkap" placeholder="ex. Rico Alex Sandra" autofocus>
                            </div>

                            <div class="form-group">
                                <label> NIK </label>
                                <input type="number" class="form-control" name="NIK" placeholder="ex. 6171011011341313">
                            </div>

                            <div class="form-group">
                                <label> Tanggal Lahir </label>
                                <input type="date" class="form-control" name="TglLahir" placeholder="ex.11 Januari 2021">
                            </div>

                            <div class="form-group">
                                <label> Jenis Kelamin </label> <br>
                                <label class="radio-inline">
                                    <input type="radio" class="radio" name="JK" value="Pria" checked> Pria 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" class="radio" name="JK" value="Wanita"> Wanita
                                </label>
                            </div>

                            <div class="form-group">
                                <label> Alamat </label>
                                <textarea class="form-control" rows="3" name="Alamat" placeholder="ex.Jln.Kemuningan"></textarea>
                            </div>

                            <div class="form-group">
                                <label> No.Telp </label>
                                <input type="number" class="form-control" name="Telp" placeholder="ex.089663759631">
                            </div>

                            <div class="form-group">
                                <label> Email </label>
                                <input type="email" class="form-control" name="Email" placeholder="ex.user@gmail.com">
                            </div>
                   
                        <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                   	</div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
