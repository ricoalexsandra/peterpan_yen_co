@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
              @foreach($pem_stat4 as $ds)
            <div class="card">
            	<div class="card-header mb-3"> <h4> <b> Rincian Pemesanan {{ $loop->iteration }}</b> </h4> </div>
                <h5 class="display-10"><p class="text-center"> <b> Rincian Pembayaran </b> </p> </h5>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif
                <table class="table table-hover">
                  <tr>
                    <td><b> Kode Transaksi </b></td>
                    <td> : </td>
                    <td> TRANS-0{{ $ds->kode_transaksi }} </td>
                  </tr>
                  <tr>
                    <td><b> Jenis Tes </b></td>
                    <td> : </td>
                    <td> 
                      {{ DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $ds->kode_detil)->value('kode_jadwal'))->value('kode_tes'))->value('nama_tes') }} 
                    </td>
                  </tr>
                  <tr>
                    <td><b> Tanggal Pemesanan </b></td>
                    <td> : </td>
                    <td> {{ $ds->getCreatedAtAttribute($ds->tgl_pemesanan) }} </td>
                  </tr>
                  <tr>
                    <td><b> Jadwal Tes </b></td>
                    <td> : </td>
                    <td>  
                      {{ $ds->getCreatedAtAttribute(DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $ds->kode_detil)->value('kode_jadwal'))->value('tgl_pelaksanaan')) }} Pukul: {{ DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $ds->kode_detil)->value('kode_jadwal'))->value('jam_pelaksanaan') }}
                    </td>
                  </tr>
                  <tr>
                    <td><b> Total Bayar </b></td>
                    <td> : </td>
                    <td> {{ $ds->total }} </td>
                  </tr>
                  <tr>
                    <form method="post" action="{{route('bayar')}}" enctype="multipart/form-data">
                      <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                      <input type="hidden" name="kode" value="{{$ds->kode_transaksi}}">
                      @method('PUT')
                      <td><b> Metode Pembayaran </b></td>
                      <td> : </td>
                      <td>  
                        <select class="form-control" name="MetodeBayar">
                          <option disabled>--Pilih Metode Pembayaran--</option>
                          <option value="BRI"> BRI </option>
                          <option value="BNI"> BNI </option>
                          <option value="BCA"> BCA </option>
                          <option value="OVO"> OVO </option>
                          <option value="GO-PAY"> GO-PAY </option>
                          <option value="SHOPEE"> SHOPEE PAY </option>
                          <option value="LINK"> LINK AJA </option>
                          <option value="DANA"> DANA </option>
                        </select>
                      </td>
                  </tr>
                  <tr>
                    <input type="hidden" name="TglBayar" value="{{$ds->updated_at}}">
                    <td><b> Upload bukti pembayaran (.pdf) </b></td>
                    <td> : </td>
                    <td> <input type="file" name="FileBayar" class="form-control"> </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <button type="submit" class="btn btn-success"> Bayar </button> 
                    </td>
                  </tr>
                </table>
                  @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
