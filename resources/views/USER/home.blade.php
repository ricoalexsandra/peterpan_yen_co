@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <h4> <b> Beranda </b></h4> </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                    @endif

                    @if(\Session::has('Forbidden'))
                            <div class="alert alert-danger">
                                <p>{{\Session::get('Forbidden')}}</p>
                            </div>
                    @endif

                    <div class="container">
                        <div class="form-group">
                            <form>
                                <fieldset disabled>
                                  <legend>Selamat, Anda berhasil Registrasi pada Sistem Psikologi Online</legend>
                                  <div class="form-group">
                                    <label for="disabledTextInput">Username</label>
                                    <input type="text" id="disabledTextInput" class="form-control" placeholder="kerenkezia12">
                                  </div>
                                  <div class="form-group">
                                    <label for="disabledTextInput">Password</label>
                                    <input type="text" id="disabledTextInput" class="form-control" placeholder="********">
                                  </div>
                                </fieldset>
                              </form>
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                  Tampilkan Password
                                </label>
                              </div>
                              <button type="button" class="btn btn-primary">Daftar Tes</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
