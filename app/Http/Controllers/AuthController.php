<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function tampilanLogin(Request $request)
    {
        return view('login');
    }

    public function prosesLogin(Request $request)
    {
        /*$request->validate([
            'username' => 'required|min:5|max:20',
            'password' => 'required|min:5'
        ]);

        $username = $request->username;
        $password = $request->password;

        if(Auth::atempt(['username' => $username, 'password' => $password])) {
            Auth::logout();
        } else {
            return back()->with('message', ['danger' => 'Username atau password salah!']);
        };*/
        return view('homeuser');
    }

    public function tampilanRegister(Request $request)
    {
        return view('register');
    }

    public function prosesRegister(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:user|min:5|max:20',
            'password' => 'required|min:5',
            'confirmpassword' => 'required|same:password',
            'email' => 'required|email:rfc,dns,filter',
            'notelp' => 'required|numeric'
        ]);

        $username = $request->username;
        $password = $request->password;
        $email = $request->email;
        $notelp = $request->notelp;

        $user = new User();
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->no_telp = $notelp;
        $query = $user->save();

        if($query) {
            return redirect('login')->with('message', ['success' => 'Berhasil registrasi!']);
        } else {
            return back()->with('message', ['danger' => 'Gagal registrasi!']);
        }
    }
}
