@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	<div class="card-header"> <h4> <b> Jenis Tes Psikologi </b></h4></div>
                <div class="container">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                    @endif
                        
                    @foreach($jenis_tes as $jt)
                    <div class="jumbotron">
                        <h5> 
                            <b> {{ $loop->iteration }}. <?php echo $jt->nama_tes ?> </b>
                            <a class="btn btn-primary" href="{{ route('jadwaltes',['kode_tes'=>$jt->kode_tes]) }}"> PILIH JADWAL</a>
                        </h5>
                        <hr class="my-4">
                        <h5> <b> Keterangan </b> </h5>
                        <h6> {{ $jt->keterangan }}</h6>
                        <h5> <b> Fasilitas </b> </h5>
                        <h6> {{ $jt->fasilitas }} </h6> 
                        <b> Biaya </b> </h5>
                        <h6> Rp. {{ number_format($jt->biaya) }} </h6>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
