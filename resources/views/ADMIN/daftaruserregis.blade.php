@extends('layouts.appadmin_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	<div class="card-header"> <h4> <b> Daftar User Registrasi </b> </h4> </div>
               @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif

                  @if(\Session::has('success'))
                      <div class="alert alert-success">
                          <p>{{\Session::get('success')}}</p>
                      </div>
                  @endif

                  @if(\Session::has('Forbidden'))
                          <div class="alert alert-danger">
                              <p>{{\Session::get('Forbidden')}}</p>
                          </div>
                  @endif
            <div class="container">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col" class="text-center"> NO </th>
                        <th scope="col" class="text-center"> USERNAME </th>
                        <th scope="col" class="text-center"> EMAIL </th>
                        <th scope="col" class="text-center">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($regis as $no => $r)
                      <tr>
                        <th scope="row"> <?php echo ++$no + (($regis->currentPage()-1)*$regis->perPage()) ?> </th>
                        <td hidden> <?php echo $r->id ?> </td>
                        <td> <?php echo $r->name ?> </td>
                        <td> <?php echo $r->email ?> </td>
                        <td>
                          <a href="/delete_regis/{{ $r->id }}" class="btn btn-danger"> Delete </a>
                        </td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>

                  <p> Halaman : </p>
                  @php echo $regis->links() @endphp

            </div>
        </div>
    </div>
</div>
@endsection
