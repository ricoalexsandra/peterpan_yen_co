<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Login Admin dan User
Route::get('/homeadmin', [App\Http\Controllers\HomeController::class, 'homeadmin'])->name('homeadmin')->middleware('is_admin');
<<<<<<< HEAD
Route::get('/homeuser', [App\Http\Controllers\HomeController::class, 'index'])->name('homeuser');
//Route::get('/homeuser', [App\Http\Controllers\HomeController::class, 'homeuser'])->name('homeuser');
=======
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
>>>>>>> 8b63d17a93d7d457d70186b4195ea4113cf130ec

//User
Route::get('/pendaftaran', 'App\Http\Controllers\PendaftaranController@index')->name('pendaftaran');
Route::get('/statusTes', 'App\Http\Controllers\StatusController@index')->name('statusTes');
Route::get('/biodata', 'App\Http\Controllers\BiodataController@index')->name('biodata');
Route::get('/jenistes', 'App\Http\Controllers\JenistesController@index')->name('jenistes');
Route::get('/jadwaltes/{kode_tes}', 'App\Http\Controllers\JadwaltesController@index')->name('jadwaltes');


Route::post('/intes', 'App\Http\Controllers\InputTesController@create')->name('intes');

Route::get('/tambahinputtes', 'App\Http\Controllers\InputTesController@tambah')->name('tambahinputtes');
Route::get('/editinputtes/{kode_tes}', 'App\Http\Controllers\InputTesController@edit')->name('editinputtes');
Route::put('/updateinputtes/{kode_tes}', 'App\Http\Controllers\InputTesController@update')->name('updateinputtes');


Route::get('/delete_regis/{id}', 'App\Http\Controllers\DaftarUserRegisController@delete')->name('delete_regis');

Route::get('/deleteinputtes/{kode_tes}', 'App\Http\Controllers\InputTesController@delete')->name('deleteinputtes');

Route::get('/tambahinputjadwal', 'App\Http\Controllers\InputJadwalController@tambah')->name('tambahinputjadwal');
Route::get('/editinputjadwal/{kode_jadwal}', 'App\Http\Controllers\InputJadwalController@edit')->name('editinputjadwal');
Route::put('/updateinputjadwal/{kode_jadwal}', 'App\Http\Controllers\InputJadwalController@update')->name('updateinputjadwal');
Route::get('/deleteinputjadwal/{kode_jadwal}', 'App\Http\Controllers\InputJadwalController@delete')->name('deleteinputjadwal');

Route::post('/injadwal', 'App\Http\Controllers\InputJadwalController@create')->name('injadwal');

Route::post('/inpendaftaran', 'App\Http\Controllers\PendaftaranController@create')->name('inpendaftaran');
Route::post('/biodataupdate/{id}', 'App\Http\Controllers\BiodataController@update')->name('biodataupdate');


Route::post('/pilihJadwal', 'App\Http\Controllers\JadwaltesController@pilihJadwal')->name('pilihJadwal');




Route::get('/pembayaran', 'App\Http\Controllers\PembayaranController@index')->name('pembayaran');

Route::post('/lihatFile', 'App\Http\Controllers\VerifPembayaranController@lihatFile')->name('lihatFile');


//Admin
Route::get('/daftarpeserta', 'App\Http\Controllers\DaftarPeserta@index')->name('daftarpeserta');
Route::get('/daftaruserregis', 'App\Http\Controllers\DaftarUserRegisController@index')->name('daftaruserregis');
Route::get('/inputtes', 'App\Http\Controllers\InputTesController@index')->name('inputtes');

Route::get('/inputjadwal', 'App\Http\Controllers\InputJadwalController@index')->name('inputjadwal');
Route::post('/verifikasiPeserta', 'App\Http\Controllers\VerifPesertaController@index')->name('verifikasiPeserta');
Route::post('/tolakPeserta', 'App\Http\Controllers\VerifPesertaController@tolak')->name('tolakPeserta');

Route::put('/bayar', 'App\Http\Controllers\PembayaranController@bayar')->name('bayar');
Route::get('/verifpembayaran', 'App\Http\Controllers\VerifPembayaranController@index')->name('verifpembayaran');
Route::post('/verifbayar', 'App\Http\Controllers\VerifPembayaranController@verifbayar')->name('verifbayar');
