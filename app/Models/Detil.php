<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Detil extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode_detil','kode_peserta', 'kode_jadwal','kode_tes','status_tes', 'id','created_at','updated_at',
    ];
    protected $table ='tb_detil_baru';

    public function getCreatedAtAttribute($value){
    	$date = new Carbon($value);
    	return $date->format('l, d-M-Y');
    }
}
