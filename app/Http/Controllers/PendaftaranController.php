<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pendaftaran;
use Auth;
use App\Models\User;

class PendaftaranController extends Controller
{
    public function index(){
    	return view('pendaftaran');
    }

    public function create(Request $req){
        $this->validate($req, [
            'NamaLengkap' => 'required',
            'NIK' => 'required',
            'TglLahir' => 'required',
            'JK' => 'required',
            'Alamat' => 'required',
            'Telp' => 'required',
            'Email' => 'required'
        ]);

        $val = User::where([
            ['nik', '=', $req->get('NIK')],
        ])->value('id');

        if($val){
            return redirect()->route('home')->with('Forbidden','Anda sudah Terdaftar!');
        }else{
            $Pendaftaran=new Pendaftaran([
                'nama_lengkap' => $req->get('NamaLengkap'),
                'nik' => $req->get('NIK'),
                'tgl_lahir' => $req->get('TglLahir'),
                'jk' => $req->get('JK'),
                'alamat' => $req->get('Alamat'), 
                'telp' => $req->get('Telp'), 
                'email' => $req->get('Email')
            ]);
            $Pendaftaran->save();

            $update = [
                'nik' => $req->get('NIK'),
            ];
            User::where('id', $req->get('id'))->update($update);

            return redirect()->route('jenistes')->with('success','Anda berhasil mendaftar sebagai peserta, silahkan pilih Jenis Tes');
        }
    }
}
