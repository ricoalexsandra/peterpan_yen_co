@extends('layouts.appadmin_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
            	<div class="card-header"> <h4> <b> Pembayaran </b><h4> </div>

                    <div class="container">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">NO</th>
                                    <th scope="col" class="text-center">NAMA</th>
                                    <th scope="col" class="text-center">TANGGAL LAHIR</th>
                                    <th scope="col" class="text-center">JENIS KELAMIN</th>
                                    <th scope="col" class="text-center">JENIS TES</th>
                                    <th scope="col" class="text-center">JADWAL</th>
                                    <th scope="col" class="text-center">BUKTI PEMBAYARAN</th>
                                    <th scope="col" class="text-center">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Keren Kezia</td>
                                <td>10 Mei 2000</td>
                                <td>Perempuan</td>
                                <td>Grafis</td>
                                <td>Senin, 10 Mei 2021</td>
                                <td class="text-center"><button><span class="fa fa-check-circle"></span></button></td>
                                <td><a class="btn btn-primary btn-lg" href="#" role="button">VERIFIKASI</a></td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Rico Alex Sandra</td>
                                <td>10 November 2000</td>
                                <td>Lakilaki</td>
                                <td>DISC</td>
                                <td>Selasa, 11 Mei 2021</td>
                                <td class="text-center"><button><span class="fa fa-check-circle"></span></button></td>
                                <td><a class="btn btn-primary btn-lg" href="#" role="button">VERIFIKASI</a></td>
                            </tr>
                            </tbody>
                        </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
