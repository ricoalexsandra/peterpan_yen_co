@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">  
            <div class="card">
                <div class="card-header"> <b> Update Biodata </b> </div> <br>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif
                <div class="container"> 
                	<div class="form-group">
                            @foreach($data as $d)
                        <form method="post" action="{{route('biodataupdate',['id'=>$d->nik])}}" enctype='multipart/form-data'>
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$d->nik}}">
                                <label> Nama Lengkap </label>
                                <input type="text" class="form-control" name="NamaLengkap" value="{{ $d->nama_lengkap }}" autofocus>
                            </div>

                            <div class="form-group">
                                <label> NIK </label>
                                <input type="number" class="form-control" name="NIK" value="{{ $d->nik }}" disabled>
                            </div>

                            <div class="form-group">
                                <label> Tanggal Lahir </label>
                                <input type="date" class="form-control" name="TglLahir" value="{{ $d->tgl_lahir }}">
                            </div>

                            <div class="form-group">
                                <label> Jenis Kelamin </label> <br>
                                <label class="radio-inline">
                                    <input type="radio" class="radio" name="JK" value="Pria"  @php if($d->jk=='Pria') echo 'checked' @endphp> Pria 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" class="radio" name="JK" value="Wanita"  @php if($d->jk=='Wanita') echo 'checked' @endphp> Wanita
                                </label>
                            </div>

                            <div class="form-group">
                                <label> Alamat </label>
                                <textarea class="form-control" rows="3" name="Alamat"> {{ $d->alamat }} </textarea>
                            </div>

                            <div class="form-group">
                                <label> No.Telp </label>
                                <input type="number" class="form-control" name="Telp" value="{{ $d->telp }}">
                            </div>

                            <div class="form-group">
                                <label> Email </label>
                                <input type="email" class="form-control" name="Email" value="{{ $d->email }}">
                            </div>
                            @endforeach
                        <button type="submit" class="btn btn-primary"> Update </button>

                        </form>
                   	</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
