@extends('layouts.appadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header mb-3"><h4><b> Data Jenis Tes </b></h4></div>
                <div class="container">
                    <form method="post" action="{{route('intes')}}" enctype='multipart/form-data'>
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                        @endif

                        @if(\Session::has('Forbidden'))
                            <div class="alert alert-danger">
                                <p>{{\Session::get('Forbidden')}}</p>
                            </div>
                        @endif
                        
                        <div class="form-group">
                            <label><b> Nama Tes </b></label>
                            <input type="text" class="form-control" name="NamaTes" placeholder="ex. DISC">
                        </div>
                        <div class="form-group">
                            <label><b> Fasilitas </b></label> <br>
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Sertifikat"> Sertifikat <br>
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Surat Keterangan"> Surat Keterangan Tes <br>
                            <input type="checkbox" name="Fasilitas[]" class="checkbox" value="Penjelasan Hasil Tes"> Penjelasan Hasil Tes <br>
                        </div>
                        <div class="form-group">
                            <label><b> Biaya </b></label>
                            <input type="text" class="form-control" name="Biaya" placeholder="ex.150000">
                        </div>
                        <div class="form-group">
                            <label><b> Keterangan Jenis Tes </b></label>
                            <textarea name="Keterangan" rows="3" class="form-control" placeholder="ex. Jenis tes untuk mengetahui kepribadian seseorang"></textarea>
                        </div>
                        <!-- <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Pelaksanaan</label>
                            <input type="date" class="form-control" >
                        </div> -->
                        <button type="submit" class="btn btn-primary mb-3"> Simpan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection