@extends('layouts.appadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            	<div class="card-header"> <h4> <b> Data Pembayaran </b><h4> </div>
                 @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif 

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                    @endif
            <div class="container">
                <table class="table table-hover">
                    <tr>
                        <th scope="col" class="text-center"> No</th>
                        <th scope="col" class="text-center"> Kode Transaksi</th>
                        <th scope="col" class="text-center"> Nama</th>
                        <th scope="col" class="text-center"> Jenis Tes</th>
                        <th scope="col" class="text-center"> Jadwal Tes </th>
                        <th scope="col" class="text-center"> File Pembayaran </th>
                        <th scope="col" class="text-center"> Status </th>
                        <th scope="col" class="text-center"> Action </th>
                    </tr>
                    @foreach($pem_stat_verif6 as $dp)
                        <tr>
                            <td> {{ $loop->iteration }} </td>
                            <td> TRANS-{{ $dp->kode_transaksi }} </td>
                            <td> 
                                {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('id'))->value('nik'))->value('nama_lengkap') }} 
                            </td>
                            <td> 
                                {{ DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('kode_tes'))->value('nama_tes') }}
                            </td>
                            <td>
                                {{ $dp->getCreatedAtAttribute(DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('tgl_pelaksanaan')) 
                                }} Pukul: {{ DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('jam_pelaksanaan') }}
                            </td>
                            <td>
                                <form method="post" action="{{route('lihatFile')}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    
                                    <input type="hidden" name="id" value="{{$dp->file_pembayaran}}" >
                                    <button type="submit" class="btn btn-link"> 
                                    {{$dp->file_pembayaran}} </button>
                                </form>
                            </td>
                            <td>
                                @if($dp->status_pembayaran==1)
                                    <span class="fa fa-clock-o" style="font-size:24px"></span>
                                @elseif($dp->status_pembayaran==2)
                                    <span class="fa fa-check" style="font-size:24px"></span>
                                @else
                                    Ditolak 
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{ $loop->iteration }}"> Verifikasi </button>

                                <!-- The Modal -->
                                <div class="modal fade" id="myModal{{ $loop->iteration }}">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                      
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                          <h4 class="modal-title"> Yakin ingin Verifikasi ?</h4>
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <table class="table">
                                                <tr>
                                                    <td><b> Kode Transaksi</b></td>
                                                    <td> : </td>
                                                    <td> TRANS-{{ $dp->kode_transaksi 
                                                    }} </td>
                                                </tr>
                                                <tr>
                                                    <td><b> Nama Lengkap </b></td>
                                                    <td> : </td>
                                                    <td> 
                                                        {{ DB::table('tb_peserta')->where('nik', DB::table('users')->where('id', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('id'))->value('nik'))->value('nama_lengkap') }} 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b> Jenis Tes </b></td>
                                                    <td> : </td>
                                                    <td> 
                                                        {{ DB::table('tb_jenis_tes')->where('kode_tes', DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('kode_tes'))->value('nama_tes') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b> Jadwal Pelaksanaan </b></td>
                                                    <td> : </td>
                                                    <td>
                                                        {{ $dp->getCreatedAtAttribute(DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('tgl_pelaksanaan')) 
                                                        }} Pukul: {{ DB::table('tb_jadwal')->where('kode_jadwal', DB::table('tb_detil_baru')->where('kode_detil', $dp->kode_detil)->value('kode_jadwal'))->value('jam_pelaksanaan') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Link Pertemuan </td>
                                                    <td> : </td>
                                                    <td> <input type="text" name="link" class="form-control"> </td>
                                                </tr>
                                            </table>
                                        </div>
                                        
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <form method="post" action="{{route('verifbayar')}}">

                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <input type="hidden" name="kode" value="{{$dp->kode_transaksi}}">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal </button>
                                                <button type="submit" class="btn btn-success"> Konfirmasi </button>
                                            </form>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection
