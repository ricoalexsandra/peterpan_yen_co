<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputJadwal extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode_tes', 'nama_tes', 'kode_jadwal', 'jam_pelaksanaan', 'tgl_pelaksanaan', 'link_pertemuan','kapasitas', 'created_at','updated_at',
    ];
    protected $table ='tb_jadwal';
}
