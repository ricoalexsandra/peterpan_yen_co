<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisTes extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode_tes', 'nama_tes', 'biaya', 'fasilitas', 'keterangan', 'kode_jadwal', 'jam_pelaksanaan', 'tgl_pelaksanaan', 'link_pertemuan','kapasitas', 'created_at','updated_at',
    ];

    protected $table ='tb_jenis_tes';
    protected $primaryKey = 'kode_tes';

    public function getCreatedAtAttribute($value){
    	$date = new Carbon($value);
    	return $date->format('l, d-m-Y');
    }
}
