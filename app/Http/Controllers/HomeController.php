<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->nik==null)
        {
            return redirect()->to('/pendaftaran');
        }
        else
        {
            return view('homeuser');
        }
    }

    public function homeadmin(){
        return view('homeadmin');
    }
}
