<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pendaftaran;
use Auth;

class BiodataController extends Controller
{
    public function index(){
    	$auth = Auth::user()->id;
    	$data = Pendaftaran::join('users', 'users.nik', '=', 'tb_peserta.nik')->select('users.id','tb_peserta.*')->where('id', $auth)->get();
    	return view('biodata', compact('data'));
    }

    public function update($id, Request $req){
    	$up = Pendaftaran::find($id);
            $up->nama_lengkap = $req->get('NamaLengkap');
            $up->tgl_lahir = $req->get('TglLahir');
            $up->jk = $req->get('JK');
            $up->alamat = $req->get('Alamat');
            $up->telp = $req->get('Telp');
            $up->email = $req->get('Email');
            $up->save();
    	return redirect()->route('biodata')->with('success','Data Updated');
    }
}
