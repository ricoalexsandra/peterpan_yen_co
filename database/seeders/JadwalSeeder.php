<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Jadwal;


class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jadwal = [
        	[
                'kode_jadwal' => '1',
	        	'kode_tes' => '1',
	        	'jam_pelaksanaan' => '10:00 AM',
	        	'tgl_pelaksanaan' => '2020-10-10',
	        	'link_pertemuan' => 'blbalba',
	        	'kapasitas' => '20',
        	],
        	[
                'kode_jadwal' => '1',
	        	'kode_tes' => '2',
	        	'jam_pelaksanaan' => '11:00 AM',
	        	'tgl_pelaksanaan' => '2020-11-11',
	        	'link_pertemuan' => 'blbalba',
	        	'kapasitas' => '25',
        	]
        ];

        foreach($jadwal as $key => $value){
        	Jadwal::create($value);
        }
    }
}
