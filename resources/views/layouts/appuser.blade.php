<!DOCTYPE html>
	<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Script -->
		<script>
			function openNav() {
			  document.getElementById("mySidebar").style.width = "250px";
			  document.getElementById("main").style.marginLeft = "250px";
			}

			function closeNav() {
			  document.getElementById("mySidebar").style.width = "0";
			  document.getElementById("main").style.marginLeft= "0";
			}
		</script>

		<!-- Style -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ asset('css/app.css')}}">

		<style>
			body {
				font-family: "Lato", sans-serif;
			}

			.sidebar {
			  height: 100%;
			  width: 0;
			  position: fixed;
			  z-index: 1;
			  top: 0;
			  left: 0;
			  background-color: rgb(0, 0, 0);
			  overflow-x: hidden;
			  transition: 0.5s;
			  padding-top: 60px;
			}

			.sidebar a {
              padding: 8px 8px 8px 32px;
              text-decoration: none;
              font-size: 25px;
              color: #18db90;
              display: block;
              transition: 0.3s;
            }

            .sidebar a:hover {
              color: #ffffff;
            }
			}

			.sidebar .closebtn {
			  position: absolute;
			  top: 0;
			  right: 25px;
			  font-size: 36px;
			  margin-left: 50px;
			}

			.openbtn {
			  font-size: 20px;
			  cursor: pointer;
			  background-color: #111;
			  color: #18db90;
			  padding: 10px 15px;
			  border: none;
			}

			.openbtn:hover {
			  background-color: rgb(252, 252, 252);
			}

			#main {
			  transition: margin-left .5s;
			  padding: 16px;
			}

			/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
			@media screen and (max-height: 450px) {
			  .sidebar {padding-top: 15px;}
			  .sidebar a {font-size: 18px;}
			}
		</style>
	</head>
	<body>
		<div id="mySidebar" class="sidebar">
        <h3 style="color: #32f15ba6"> Welcome {{ Auth::user()->name }} </h3> <span class="caret"> </span>
		  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
		  <a href="#"> <span class="fa fa-drivers-license-o"> Beranda </a>
		  <a href="{{ route('pendaftaran') }}"><span class="fa fa-address-book"> Pendaftaran </a>
		  <a href="{{ route('jenistes') }}"><span class="fa fa-calendar"> Jenis Tes </a>
		  <a href="{{ route('pembayaran') }}"> <span class="fa fa-money"> Pembayaran </a>
          {{-- <a href="{{ route('Keluar') }}"> <span class="fa fa-money">Keluar </a> --}}
          {{-- <a href="{{ route('homeadmin') }}"> <span class="fa fa-money">Beranda </a> --}}
          <a href="{{ route('daftaruserregis') }}"> <span class="fa fa-check-square-o"> Daftar User Registrasi </a>
          <a href="{{ route('daftarpeserta') }}"> <span class="fa fa-child"> Daftar Peserta Tes </a>
          <a href="{{ route('inputtes') }}"> <span class="fa fa-cloud-download"> Input Jenis Tes </a>
          <a href="{{ route('inputjadwal') }}"> <span class="fa fa-calendar"> Input Jadwal </a>
          <a href="{{ route('verifpembayaran') }}"> <span class="fa fa-check-square"> Verifikasi Pembayaran </a>
          {{-- <a href="{{ route('Keluar') }}"> <span class="fa fa-money">Keluar </a> --}}
		</div>
		<div id="main">
			<button class="openbtn" onclick="openNav()"> ☰ Menu </button>
			<main class="py-4">
            	@yield('content')
        	</main>
		</div>

	</body>
</html>
