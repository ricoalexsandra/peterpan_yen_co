<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Detil;

class DaftarPeserta extends Controller
{
    public function index(){
    	$detil= Detil::where([
            ['status_tes', '=', '0'],
            ])->simplePaginate(5);  
    	return view('daftarpeserta',compact('detil'));
    	
    }
}
