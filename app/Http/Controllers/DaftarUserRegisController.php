<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class DaftarUserRegisController extends Controller
{
    public function index(){
    	$regis = User::where([
            ['is_admin', '=', null],
            ])->simplePaginate(10);  
    	return view('daftaruserregis',compact('regis'));
    }

    public function delete($id){
    	$del_user = User::find($id);
        $del_user->delete();
        return redirect()->route('daftaruserregis')->with('forbidden','Data deleted');
    }
}
