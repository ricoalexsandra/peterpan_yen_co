    <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Peserta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*//Create Table Peserta
        Schema::create('tb_peserta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_peserta');
            $table->char('nama_lengkap');
            $table->integer('nik');
            $table->date('tgl_lahir');
            $table->char('jk');
            $table->char('alamat');
            $table->char('telp');
            $table->char('email');
            $table->timestamps();
        });

        //Create Table Jenis Tes
        Schema::create('tb_jenis_tes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_tes');
            $table->char('nama_tes');
            $table->integer('biaya');
            $table->char('keterangan');
            $table->timestamps();
        });

        //Create Table Jadwal
        Schema::create('tb_jadwal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_jadwal');
            $table->foreignId('kode_tes');
            $table->char('jam_pelaksanaan');
            $table->date('tgl_pelaksanaan');
            $table->char('link_pertemuan');
            $table->integer('kapasitas');
            $table->timestamps();
        });

        Schema::table('tb_jadwal', function(Blueprint $table){
            $table->foreign('kode_tes')->references('id')->on('tb_jenis_tes')->onDelete('restrict')->onUpdate('restrict');
        });

        //Create Table Transaksi
        Schema::create('tb_transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_transaksi');
            $table->foreignId('kode_peserta');
            $table->foreignId('kode_jadwal');
            $table->date('tgl_pembayaran');
            $table->char('file_pembayaran');
            $table->integer('subtotal');
            $table->timestamps();
        });

        Schema::table('tb_transaksi', function(Blueprint $table){
            $table->foreign('kode_peserta')->references('id')->on('tb_peserta')->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('tb_transaksi', function(Blueprint $table){
            $table->foreign('kode_jadwal')->references('id')->on('tb_jadwal')->onDelete('restrict')->onUpdate('restrict');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_peserta');
        Schema::dropIfExists('tb_jenis_tes');
        Schema::dropIfExists('tb_transaksi');
        Schema::dropIfExists('tb_jadwal');

    }
}
