@extends('layouts.appuser_new')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	<div class="card-header mb-3"> <h4> <b> Rincian Pemesanan </b> </h4> </div>
                <h5 class="display-10"><p class="text-center"> <b> Rincian Pembayaran </b> </p> </h5>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

<<<<<<< HEAD:resources/views/USER/pembayaran.blade.php
        <div class="col-md-6">
            <div class="card">
              <div class="card-header mr-3"> <h4> <b>Alur Tes </b> </h4></div>
              <h4 class="display-10"><p class="text-center"> <b> Alur Tes </b> </p> </h4>
                        <h6>Keterangan Tes: Tes Selesai</h6>
                        <h6> <b>Unduh Hasil Tes <b> </h6>
                        <td><a class="btn btn-primary" href="#" role="button"><b> UNDUH </b></a></td>
=======
                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{\Session::get('success')}}</p>
                    </div>
                @endif
                <table class="table table-hover">
                  <tr>
                    
                    @foreach($detil_stat as $ds)
                      @if($ds->status_tes==0)
                        <td> Belum ada </td>
                      @else
                        @foreach($pem_stat4 as $pem4)
                        <td><b> Kode transksi </b></td>
                        <td> : </td>
                        <td> TRANS-0{{ $pem4->kode_transaksi }} </td>
                      </tr>
                      <tr>
                        <td><b> Jenis Tes </b></td>
                        <td> : </td>
                        @foreach($pem_stat3 as $pem3)
                          <td> {{ $pem3->nama_tes }} </td>
                          </tr>
                          <tr>
                            <td><b> Tanggal Pemesanan </b></td>
                            <td> : </td>
                            <td> {{ $pem4->getCreatedAtAttribute($pem4->tgl) }} </td>
                          </tr>
                            @foreach($pem_stat2 as $pem2)
                              <tr>
                                <td><b> Jadwal Tes </b></td>
                                <td> : </td>
                                <td> {{ $pem2->getCreatedAtAttribute($pem2->tgl_pelaksanaan) }} </td>
                              </tr>
                              <tr>
                                <td><b> Jam Pelaksanaan </b></td>
                                <td> : </td>
                                <td> {{ $pem2->jam_pelaksanaan }} </td>
                              </tr>
                            @endforeach
                          <tr>
                            <td><b> Total Bayar </b></td>
                            <td> : </td>
                            <td> Rp.{{ number_format($pem3->biaya) }} </td>
                        @endforeach
                      </tr>
                      <tr>
                        <td><b> Metode Pembayaran </b></td>
                        <td> : </td>
                        <td>  
                          <select class="form-control" name="MetodeBayar">
                            <option disabled>--Pilih Metode Pembayaran--</option>
                            <option value="BRI"> BRI </option>
                            <option value="BNI"> BNI </option>
                            <option value="BCA"> BCA </option>
                            <option value="OVO"> OVO </option>
                            <option value="GO-PAY"> GO-PAY </option>
                            <option value="SHOPEE"> SHOPEE PAY </option>
                            <option value="LINK"> LINK AJA </option>
                            <option value="DANA"> DANA </option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <input type="hidden" name="TglBayar" value="{{$pem4->updated_at}}">
                      </tr>
                      <tr>
                        <td><b> Upload bukti pembayaran (.pdf) </b></td>
                        <td> : </td>
                        <td> <input type="file" name="FileBayar" class="form-control" enctype="multipart/form-data"> </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                        <form method="post" action="/bayar/{{$pem4->kode_transaksi}}">
                          <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                          @method('PUT')
                          <button type="submit" class="btn btn-success"> Bayar </button> 
                        </td>
                      </form>
                      </tr>
                    </table>
                        @endforeach
                      @endif
                  @endforeach
>>>>>>> 8362e4829313ccf2ab7db935f898ea109e159c20:resources/views/pembayaran.blade.php
            </div>
        </div>
    </div>
</div>
@endsection
