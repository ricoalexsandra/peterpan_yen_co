@extends('layouts.appadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header mb-3"><h4><b> Data Jenis Tes </b></h4></div>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                    @endif

                    @if(\Session::has('Forbidden'))
                            <div class="alert alert-danger">
                                <p>{{\Session::get('Forbidden')}}</p>
                            </div>
                    @endif
                <div class="container">
                    <blockquote class="blockquote">
                        <a href="/tambahinputtes" class="btn btn-outline-primary"> Jenis Tes [+] </a>
                    </blockquote>
                    <table class="table table-hover">
                        <tr>
                            <th> No </th>
                            <th> Nama Tes </th>
                            <th> Fasilitas </th>
                            <th> Biaya </th>
                            <th> Keterangan </th>
                            <th> Action </th>
                        </tr>
                        <tr>
                            @foreach($jenis_tes as $jt)
                                <td> {{ $loop->iteration }} </td>
                                <td> {{ $jt->nama_tes }} </td>
                                <td> {{ $jt->fasilitas }} </td>
                                <td> {{ $jt->biaya }} </td>
                                <td> {{ $jt->keterangan }} </td>
                                <td> 
                                    <a href="/editinputtes/{{$jt->kode_tes}}" class="btn btn-warning"> Edit </a>
                                    <a href="/deleteinputtes/{{$jt->kode_tes}}" class="btn btn-danger"> Delete </a>
                                </td>
                        </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
