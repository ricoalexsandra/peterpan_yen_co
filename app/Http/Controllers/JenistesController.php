<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\JenisTes;

class JenistesController extends Controller
{
    public function index(){
    	$jenis_tes = JenisTes::all();
    	return view('jenistes', compact('jenis_tes'));
    }
}
