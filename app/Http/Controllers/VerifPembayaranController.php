<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Detil;
use App\Models\Transaksi;
use App\Models\User;
use App\Models\Jadwal;

class VerifPembayaranController extends Controller
{
    public function index(){
    	/*$pem_stat_verif1 = Transaksi::join('tb_detil_baru', 'tb_detil_baru.kode_detil','=','tb_transaksi.kode_detil')->select('tb_transaksi.kode_transaksi','tb_detil_baru.kode_jadwal','tb_detil_baru.id')->get();
    	$convert1 = $pem_stat_verif1[0]['kode_jadwal'];
    	$convert11 = $pem_stat_verif1[0]['id'];

    	$pem_stat_verif2 = Detil::join('tb_jadwal', 'tb_jadwal.kode_jadwal','=','tb_detil_baru.kode_jadwal')->select('tb_jadwal.kode_tes','tb_jadwal.tgl_pelaksanaan', 'tb_jadwal.jam_pelaksanaan', 'tb_jadwal.link_pertemuan')->where('tb_jadwal.kode_jadwal', $convert1)->get();
    	$convert2 = $pem_stat_verif2[0]['kode_tes'];

    	$pem_stat_verif3 = Detil::join('users', 'users.id','=','tb_detil_baru.id')->select('users.nik')->where('users.id', $convert11)->get();
    	$convert3 = $pem_stat_verif3[0]['nik'];

    	$pem_stat_verif4 = User::join('tb_peserta', 'tb_peserta.nik','=','users.nik')->select('tb_peserta.nama_lengkap')->where('tb_peserta.nik', $convert3)->get();
    	$convert4 = $pem_stat_verif4[0]['nama_lengkap'];

    	$pem_stat_verif5 = Jadwal::join('tb_jenis_tes', 'tb_jenis_tes.kode_tes','=','tb_jadwal.kode_tes')->select('tb_jenis_tes.nama_tes','tb_jenis_tes.biaya')->where('tb_jenis_tes.kode_tes', $convert2)->get();*/

       $pem_stat_verif6 = Transaksi::where([
            ['status_pembayaran', '=', '1'],
            ])->get(); 

    	return view('verifpembayaran',compact('pem_stat_verif6'));
    }

    public function verifbayar(Request $req){
        Transaksi::where('kode_transaksi', $req->get('kode'))->update(['status_pembayaran' => 2]);

        return redirect()->route('verifpembayaran')->with('success','Data Verified');
    }

    public function lihatFile(Request $req){
    	$doc = $req->get('id');
        //$from = $req->get('from');
        return view('buktipembayaran',compact('doc'));
    }
}

